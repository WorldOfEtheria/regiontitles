package net.worldofetheria.regiontitles;

/**
 * Created by rodrigo on 09/03/15.
 */
public class Utils {

    public static String Join(String[] array){
        return Join(array, " ");
    }

    public static String Join(String[] array, String separator){
        String joined = "";
        if(array.length >= 1)
            joined += array[0];

        for(int i = 1; i < array.length; i++)
            joined += separator + array[i];

        return joined;
    }

}
