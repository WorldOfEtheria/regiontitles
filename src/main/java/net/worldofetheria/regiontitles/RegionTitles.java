/**
 * Created by rmachado on 08/03/15.
 */
package net.worldofetheria.regiontitles;

import net.worldofetheria.regiontitles.hooks.AbstractHook;
import net.worldofetheria.regiontitles.hooks.FactionsHook;
import net.worldofetheria.regiontitles.hooks.WorldGuardHook;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

public class RegionTitles extends JavaPlugin implements Listener {

    private Settings settings;
    private List<AbstractHook> hooks;
    private HashMap<Player, Location> lastLocation;

    @Override
    public void onEnable(){
        getLogger().log(Level.INFO, "RegionTitles successfully enabled");
        this.saveDefaultConfig();

        settings = new Settings(this.getConfig());

        hooks = new ArrayList<AbstractHook>();

        lastLocation = new HashMap<Player, Location>();

        // Initialize the hooks
        try {
            AbstractHook wgHook = new WorldGuardHook(this, settings);
            hooks.add(wgHook);
            getLogger().log(Level.INFO, "WorldGuard hooked.");
        } catch(NoClassDefFoundError e){}

        try{
            AbstractHook fHook = new FactionsHook(this, settings);
            hooks.add(fHook);
            getLogger().log(Level.INFO, "Factions hooked.");
        } catch(NoClassDefFoundError e){}

        // Start the background task
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        scheduler.scheduleSyncRepeatingTask(this, new Runnable(){
            @Override
            public void run(){
                update();
            }
        }, 0L, settings.period);
    }

    @Override
    public void onDisable(){
        getLogger().log(Level.INFO, "RegionTitles disabled");
    }

    private void update(){
        Iterator<? extends Player> playerIterator = getServer().getOnlinePlayers().iterator();
        while(playerIterator.hasNext()){
            Player player = (Player)playerIterator.next();

            if(lastLocation.containsKey(player)){
                Iterator<AbstractHook> hookIterator = hooks.iterator();
                while(hookIterator.hasNext()){
                    AbstractHook hook = hookIterator.next();
                    hook.onPlayerUpdate(player, lastLocation.get(player), player.getLocation());
                }
            }

            lastLocation.put(player, player.getLocation());
        }
    }

    @EventHandler
    public void onPlayerLogout(PlayerQuitEvent evt) {
        Player player = evt.getPlayer();
        if(lastLocation.containsKey(player)){
            lastLocation.remove(player);
        }
    }
}
