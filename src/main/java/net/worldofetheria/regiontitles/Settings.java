package net.worldofetheria.regiontitles;

import org.bukkit.Color;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by rodrigo on 19/03/15.
 */
public class Settings {
    public int period;

    public int fadeIn;

    public int fadeOut;

    public int stay;

    public String wgSeparator;

    public String fWildernessName;

    public String fWildernessColor;

    public String fFactionColorHeading;

    public String fFactionColorSubHeading;

    public Settings(FileConfiguration config){
        this.period = config.getInt("general.period");
        this.fadeIn = config.getInt("general.fadeIn");
        this.fadeOut = config.getInt("general.fadeOut");
        this.stay = config.getInt("general.stay");

        this.wgSeparator = config.getString("worldguard.separator");

        this.fWildernessName = config.getString("factions.wilderness.name");
        this.fWildernessColor = config.getString("factions.wilderness.color");
        this.fFactionColorHeading = config.getString("factions.faction.colorHeading");
        this.fFactionColorSubHeading = config.getString("factions.faction.colorSubheading");
    }
}
