package net.worldofetheria.regiontitles.hooks;

import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.massivecore.ps.PS;
import net.md_5.bungee.api.ChatColor;
import net.worldofetheria.regiontitles.Settings;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by rodrigo on 19/03/15.
 */
public class FactionsHook extends AbstractHook {

    public FactionsHook(JavaPlugin plugin, Settings settings){
        super(plugin, settings);
    }

    private final Faction WILDERNESS = FactionColl.get().getNone();

    @Override
    public void onPlayerUpdate(Player player, Location last, Location current) {
        Faction lastFaction = BoardColl.get().getFactionAt(PS.valueOf(last));
        Faction currentFaction = BoardColl.get().getFactionAt(PS.valueOf(current));

        if(!lastFaction.equals(currentFaction)){
            if(currentFaction.equals(WILDERNESS)){
                showTitle(player, applyColor(settings.fWildernessName, settings.fWildernessColor), "");
            } else {
                String heading = applyColor(currentFaction.getName(), settings.fFactionColorHeading);
                String subheading = applyColor(currentFaction.getDescription(), settings.fFactionColorSubHeading);

                showTitle(player, heading, subheading);
            }
        }
    }

    private String applyColor(String text, String color){
        return color + text;
    }
}
