package net.worldofetheria.regiontitles.hooks;

import com.sk89q.worldedit.Vector;
import com.sk89q.worldguard.bukkit.BukkitUtil;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.worldofetheria.regiontitles.Settings;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by rodrigo on 09/03/15.
 */
public class WorldGuardHook extends AbstractHook {

    private WorldGuardPlugin worldGuard;

    public WorldGuardHook(JavaPlugin plugin, Settings settings) {
        super(plugin, settings);
        worldGuard = getWorldGuard();
    }

    @Override
    public void onPlayerUpdate(Player player, Location last, Location current) {
        HashSet<ProtectedRegion> lastRegions = getRegionsInLocation(last);
        HashSet<ProtectedRegion> currentRegions = getRegionsInLocation(current);

        Set<ProtectedRegion> entering = (Set<ProtectedRegion>)currentRegions.clone();
        entering.removeAll(lastRegions);

        Set<ProtectedRegion> leaving = (Set<ProtectedRegion>)lastRegions.clone();
        leaving.removeAll(currentRegions);

        if(entering.size() > 0){
            ProtectedRegion region = (ProtectedRegion)entering.toArray()[0];

            try {
                String message = region.getFlag(DefaultFlag.GREET_MESSAGE);

                if (message != null && !message.isEmpty()) {
                    this.showTitle(player, getHeading(message), getSubHeading(message));
                }
            } catch(NullPointerException e){}
        }
        else if(leaving.size() > 0){
            ProtectedRegion region = (ProtectedRegion)leaving.toArray()[0];

            try {
                String message = region.getFlag(DefaultFlag.FAREWELL_MESSAGE);

                if (message != null && !message.isEmpty()) {
                    this.showTitle(player, getHeading(message), getSubHeading(message));
                }
            } catch(NullPointerException e){}
        }
    }

    private WorldGuardPlugin getWorldGuard() {
        Plugin p = plugin.getServer().getPluginManager().getPlugin("WorldGuard");

        // WorldGuard may not be loaded
        if (p == null || !(p instanceof WorldGuardPlugin)) {
            return null; // Maybe you want throw an exception instead
        }

        return (WorldGuardPlugin)p;
    }

    private HashSet<ProtectedRegion> getRegionsInLocation(Location loc){
        RegionManager mgr = worldGuard.getRegionContainer().get(loc.getWorld());
        Vector vector = BukkitUtil.toVector(loc);
        Set<ProtectedRegion> regions = mgr.getApplicableRegions(vector).getRegions();

        HashSet<ProtectedRegion> copy = new HashSet<ProtectedRegion>();
        Iterator<ProtectedRegion> it = regions.iterator();
        while(it.hasNext()){
            copy.add(it.next());
        }

        return copy;
    }

    private String getHeading(String flag){
        int idx = flag.indexOf(this.settings.wgSeparator);
        if(idx > 0){
            return flag.substring(0, idx);
        }
        else {
            return flag;
        }
    };

    private String getSubHeading(String flag){
        int idx = flag.indexOf(this.settings.wgSeparator);
        if(idx > 0){
            return flag.substring(idx + 1);
        }
        else {
            return "";
        }
    }
}
