package net.worldofetheria.regiontitles.hooks;

import com.connorlinfoot.titleapi.TitleAPI;
import net.worldofetheria.regiontitles.Settings;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by rodrigo on 09/03/15.
 */
public abstract class AbstractHook{
    protected JavaPlugin plugin;
    protected Settings settings;

    public AbstractHook(JavaPlugin plugin, Settings settings){
        this.plugin = plugin;
        this.settings = settings;
    }

    public abstract void onPlayerUpdate(Player player, Location last, Location current);

    protected void showTitle(Player player, String heading, String subHeading){
        TitleAPI.sendTitle(player, settings.fadeIn, settings.stay, settings.fadeOut, heading, subHeading);
    }
}
